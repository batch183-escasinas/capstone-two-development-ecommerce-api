const mongoose=require("mongoose");	
const productSchema = new mongoose.Schema({
	name: {
		type:String,
		require:[true,"name is required"]
	}, 
	description: {
		type:String,
		require:[true,"description is required"]
	}, 
	price: {
		type:Number,
		require:[true,"price is required"]
	}, 
	stock: {
		type:Number,
		require:[true,"stock is required"]
	},
	isActive: {
		type:Boolean,
		default:true
	},
	createOn: {
		type:Date,
		default:new Date()
	},
	productImage:{
		type:Object
	}
})
module.exports=mongoose.model("Product", productSchema);