const mongoose=require("mongoose");	

const userSchema = new mongoose.Schema({
	firstName: {
		type:String,
		require:[true,"firstName is required"]
	}, 
	lastName: {
		type:String,
		require:[true,"lastName is required"]
	}, 
	email: {
		type:String,
		require:[true,"email is required"]
	}, 
	password: {
		type:String,
		require:[true,"password is required"]
	},
	isAdmin: {
		type:Boolean,
		default:false
	},
	createOn: {
		type:Date,
		default:new Date()
	}

})
module.exports=mongoose.model("User", userSchema);
