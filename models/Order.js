const mongoose=require("mongoose");	

const orderSchema = new mongoose.Schema({
	totalAmount: {
		type:Number,
		default:0
	}, 
	purchaseOn: {
		type:Date,
		default:new Date()
	},
	status: {
		type:String,
		default:"Pending"
	},
	userId: {
		type:String,
		require:[true,"userId is required"]
	},
	products: [
		

	]

})
module.exports=mongoose.model("Order", orderSchema);
