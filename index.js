const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");


//create servers
const app = express();
const port = 4000;

//middlewares
mongoose.connect("mongodb+srv://admin:admin@coursebooking.xj8c6.mongodb.net/ecommerce-app?retryWrites=true&w=majority",
	{useNewUrlParser:true,useUnifiedTopology:true});
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/uploads',express.static('uploads'));
//Routes for api
 app.use("/users",userRoutes);
app.use("/products",productRoutes);
app.use("/orders",orderRoutes);
app.listen(process.env.PORT || port, () => console.log(`API is now online on port ${port}`));