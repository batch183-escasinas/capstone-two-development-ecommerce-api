const Order=require("../models/Order");
const Product=require("../models/Product");
//const productControllers=require("../controllers/productControllers");

module.exports.viewAllOrder=()=>{
	return Order.find({}).then(result=>result);
}

module.exports.createOrder=(userId)=>{
let newOrder= new Order({
		userId: userId
	})
	return 	newOrder.save().then((product,error)=>{
		if(error){
			return false;
		}else{
			return product;
		}
	});
}
module.exports.addProductOrders= async(orderId,reqBody)=>{
	
    let productDetails=await Product.findById(reqBody.productId).then(result=>result);
	
	let subtotal=productDetails.price*reqBody.quantity;
 	 let addOrderProduct= {
 	 	productId:reqBody.productId,
 	 	subTotal:subtotal,
 	 	quantity:reqBody.quantity
 	 }
 	 if(productDetails.isActive){
 	 	 if(productDetails.stock>=reqBody.quantity){
 	 	let isOrderUpdated=await Order.findById(orderId).then(order=>{
			order.products.push(addOrderProduct);
			order.totalAmount=order.totalAmount+subtotal;
			return order.save().then((enrollment,error)=>{
				if(error){
					return false;
				}else{
					
					return true;
				}
			});
		})
 	
		let isProductUpdated=await Product.findById(reqBody.productId).then(product=>{
				product.stock-=reqBody.quantity;
				return product.save().then((enrollee,error)=>{
					if(error){
			
						return false;
					}else{
						return true;
					}
				});
		})

		if(isOrderUpdated && isProductUpdated){
				
				return true;

		}else{
				return false;

		}
 	 	//return false;
 	 }else{
 	 	return false;
 	}
 	
 	 }else{
 	 		return false;
 	 }
 	
}
module.exports.userOrder=(userId)=>{
	return Order.find({userId:userId}).then(result=>result);
}
module.exports.viewSpecificOrder=(userId,orderId)=>{
	return Order.find({ _id: orderId, userId:userId}).then(result=>result);
}