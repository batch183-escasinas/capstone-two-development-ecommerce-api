const User=require("../models/User");
const bcrypt=require("bcrypt");
const auth=require("../auth");

module.exports.registerUser=(reqBody)=>{
	let newUser= new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return 	newUser.save().then((user,error)=>{
		if(error){
			
			return false;
		}else{
			return true;
		}
	});
}
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	});
}
module.exports.getProfile = (data) =>{
	return User.findById(data.userId).then(result =>{
		result.password ="";

		return result;
	})
}
module.exports.loginUser=(reqBody)=>{
	return 	User.findOne({email:reqBody.email}).then(result => {
		if(result==null){

			return false;
		}else{	
			const isPasswordCorrect=bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {access:auth.createAccessToken(result)}

			}else{
		
				return false;
			}
		}
	});
}
module.exports.setAdminUser=async (reqBody)=>{
	let userToupdate= await User.findById(reqBody.userId).then(result=>result);
	let updateToAdmin={
		isAdmin:true
	}
	if(userToupdate.isAdmin){

		return false;
	}else{
		return User.findByIdAndUpdate(reqBody.userId,updateToAdmin).then((updateToAdmin,error)=>{
		if(error){
		
			return false;
		}else{
		
			return true;
		}
	})
	}

}

