const Product=require("../models/Product");






module.exports.createProduct=(reqBody,reqfile)=>{


let newProduct= new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stock: reqBody.stock,
		productImage: reqfile.path
	})
	return 	newProduct.save().then((product,error)=>{
		if(error){
			
			return false;
		}else{
	
			return true;
		}
	});
}
module.exports.viewAllActiveProduct=()=>{
	return Product.find({isActive:true}).then(result=>result);
}
module.exports.viewAllProduct=()=>{
	return Product.find().then(result=>result);
}

module.exports.getProduct=(productId)=>{

return Product.findById(productId).then((getProduct,errorlog)=>{
	if(errorlog){
		return false;
	}else{
		return getProduct;
	}
})
}
module.exports.updateProduct=(productId,reqBody,reqfile)=>{

let updateProduct={
	name: reqBody.name,
	description: reqBody.description,
	price: reqBody.price,
	stock: reqBody.stock,
	productImage: reqfile.path
}

return Product.findByIdAndUpdate(productId,updateProduct).then((courseUpdate,error)=>{
	if(error){
		return false;
	}else{	
		return true;
	}
})
}
module.exports.updateProductImage=(productId,reqBody)=>{
	
let updateProduct={
	name: reqBody.name,
	description: reqBody.description,
	price: reqBody.price,
	stock: reqBody.stock,
}

return Product.findByIdAndUpdate(productId,updateProduct).then((courseUpdate,error)=>{
	if(error){
		return false;
	}else{	
		return true;
	}
})
}
module.exports.archiveProduct=(productId, reqBody)=>{
let updateArchiveProduct={
		isActive : reqBody.isActive
}
return Product.findByIdAndUpdate(productId,updateArchiveProduct).then((updateArchiveProduct,error)=>{
	if(error){

		return false;
	}else{

		return true;
	}
})
}
