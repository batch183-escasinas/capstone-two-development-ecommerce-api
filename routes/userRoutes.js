const express=require("express");
const router=express.Router();
const userControllers=require("../controllers/userControllers");
const auth=require("../auth");

router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register",(req,res)=>{
	userControllers.registerUser(req.body).then(userController=>res.send(userController));

})

router.post("/login",(req,res)=>{
	userControllers.loginUser(req.body).then(userController=>res.send(userController));
})
router.post("/all-user",(req,res)=>{
	userControllers.registerUser(req.body).then(userController=>res.send(userController));
	
})
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); 
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})
router.get("/user-details/:userId", auth.verify, (req, res) =>{
	console.log(req.params.userId);
	const userData = auth.decode(req.headers.authorization); 
		if(userData.isAdmin){
			userControllers.getProfile({userId: req.params.userId}).then(resultFromController => res.send(resultFromController));

		}else{
			res.send(false);
		}
	
})





router.patch("/set-admin",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userControllers.setAdminUser(req.body).then(userController=>res.send(userController));
	}else{
		res.send(false);
	}

	
})


module.exports = router;