const express=require("express");
const router=express.Router();
const productControllers=require("../controllers/productControllers");
const auth=require("../auth");
const multer=require('multer');
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + file.originalname);
  }
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});



router.post("/create-product",upload.single('file'),auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){
			productControllers.createProduct(req.body,req.file).then(productController=>res.send(productController));
	}else{
		res.send(false);
	}
})
router.get("/viewall-active-product",(req,res)=>{
	console.log("test");
			productControllers.viewAllActiveProduct().then(productController=>res.send(productController));
})
router.get("/viewall-product",(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){
			productControllers.viewAllProduct().then(productController=>res.send(productController));
		}else{
			res.send(false);
	}
})
router.get("/:productId", (req,res)=>{
	productControllers.getProduct(req.params.productId).then(productController=>res.send(productController));
})
router.put("/:productId",upload.single('file'),auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	console.log(req.file)
	if(userData.isAdmin){
		productControllers.updateProduct(req.params.productId,req.body,req.file).then(productController=>res.send(productController));
	}else{
		res.send(false);
	}
})
router.put("/images/:productId",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.updateProductImage(req.params.productId,req.body).then(productController=>res.send(productController));
	}else{
		res.send(false);
	}
})
router.patch("/:productId/archive",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.archiveProduct(req.params.productId,req.body).then(productController=>res.send(productController));
	}else{
		res.send(false);
	}
})
module.exports = router;