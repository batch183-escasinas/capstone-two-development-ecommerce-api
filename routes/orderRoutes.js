const express=require("express");
const router=express.Router();
const orderControllers=require("../controllers/orderControllers");
const auth=require("../auth");

router.post("/create-order",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		res.send(false);			
	}else{
		orderControllers.createOrder(userData.id).then(orderController=>res.send(orderController));
	}
})		
router.post("/:orderId/add-product",auth.verify,(req,res)=>{
	console.log(req.body);
	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		
		res.send(false);			
	}else{
		orderControllers.addProductOrders(req.params.orderId,req.body).then(orderController=>res.send(orderController));
	}
})
router.get("/all-order",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		orderControllers.viewAllOrder().then(orderController=>res.send(orderController));	
	}else{
		
		res.send(false);		
			
	}
})
router.get("/user-order",auth.verify,(req,res)=>{

			

const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin){	
		res.send(false);			
	}else{
		orderControllers.userOrder(userData.id).then(orderController=>res.send(orderController));	
	}

})
router.get("/user-order/:orderId",auth.verify,(req,res)=>{
const userData=auth.decode(req.headers.authorization);
	if(userData.userData===null){	
		res.send(false);			
	}else{
		orderControllers.viewSpecificOrder(userData.id,req.params.orderId).then(orderController=>res.send(orderController));	
	}

})



module.exports = router;